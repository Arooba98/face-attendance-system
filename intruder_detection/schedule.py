import csv
from datetime import datetime

# Sample data
data = [
    ['weekdays' , 'start_time' , 'end_time',"num_of_frames"],
    ['Monday','19:00:00','10:00:00',80 ],
    ['Tuesday','19:00:00','10:00:00',80],
    ['Wednesday', '15:00:00','10:00:00',80],
    ['Thursday','10:00:00','20:00:00',80 ],
    ['Friday','19:00:00','10:00:00',80],
    ['Saturday','00:00:01','23:59:59',120],
    ['Sunday', '00:00:01','23:59:59',120]
]

# File path
file_path = 'schedule.csv'

# Create the CSV file with a header row
with open(file_path, 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(data)

print(f'CSV file "{file_path}" created successfully.')

"""

#find weekday
dt = datetime.now()
weekday=dt.strftime('%A')

def read_data_by_index(csv_file, index):
    # Open the CSV file
    with open(csv_file, 'r') as file:
        # Create a CSV reader object
        csv_reader = csv.reader(file)

        # Skip the header row if it exists
        header = next(csv_reader, None)

        # Iterate over each row in the CSV file
        for row in csv_reader:
            # Access the value at the specified index
            if row[0]==weekday:
                print(row)
                #print(type(row[1]))
                start_time=row[1]
                end_time=row[2]
    return str(start_time),str(end_time)
            # Process the value (e.g., print or store it)
# Example usage
csv_file = 'schedule.csv'
index = 0
start_time,end_time=read_data_by_index(csv_file, index)
print("start time",type(start_time))


#parse date into date type
# Use datetime.strptime to parse the time string into a datetime object
time_obj = datetime.strptime(start_time, "%H:%M:%S").time()
print(type(time_obj))  # Output: <class 'datetime.time'>
print("start time",time_obj)

exit()
# Get the current time
current_time = datetime.now().time()

if start_time <= current_time <= end_time or end_time < start_time <= current_time:
    print("The current time lies within the defined interval.")
    res=True
else:
    print("The current time is outside the defined interval.")
    res=False

print(res)"""

