# INTRUDER DETECTION SYSTEM
For building intruder detection system we first need a model to detect person in defined area.
## PERSON DETECTION  
For this purpose we will use YOLOv8n model. YOLOv8 is designed to be fast, accurate, and easy to use, making it an excellent choice for a wide range of object detection and tracking, instance segmentation, image classification and pose estimation tasks. Ultralytics HUB is providing a complete setup for easily running YOLOv8 in every environment.  
Install YOLOv8 via the ultralytics pip package for the latest stable release or by cloning the https://github.com/ultralytics/ultralytics repository for the most up-to-date version.

```
git clone https://github.com/ultralytics/ultralytics
cd ultralytics
pip install -e .
```
Use with Python
YOLOv8's Python interface allows for seamless integration into your Python projects, making it easy to load, run, and process the model's output. Designed with simplicity and ease of use in mind, the Python interface enables users to quickly implement object detection, segmentation, and classification in their projects. 
```
from ultralytics import YOLO

# Load a pretrained YOLO model (recommended for training)
model = YOLO('yolov8n.pt')

# Perform object detection on an image using the model
results = model(image)
```
After getting prediction results we will extract bounding box values and label, and draws it on the frame.  
## SMS ALERT  
We will use twillio python library to send sms alert. 
- We will first sign up to twillio account and then use twillio python library to send sms using python code. Once we finish the onboarding flow, we'll arrive at our project dashboard in the Twilio Console. This is where we'll be able to access our Account SID, authentication token, find a Twilio phone number, and more.

- Get SMS-enabled Twillio Phone Number:
Once we sign up for a free trial and verify our contact information, we can get a free Twilio phone number by clicking Get a Trial Number. Twilio will recommend a free number that we can use to test our applications.


### Python code to send message using twillio python library:
Once we have our SMS-enabled Twilio phone number and the Twilio CLI, we can proceed to install Python and the Twilio Python Helper Library.
```
import os
from twilio.rest import Client

# Find your Account SID and Auth Token at twilio.com/console and set the environment variables. See http://twil.io/secure

account_sid = os.environ["TWILIO_ACCOUNT_SID"]
auth_token = os.environ["TWILIO_AUTH_TOKEN"]
#create an object and initialize it with authentication environment variables
client = Client(account_sid, auth_token)
#create a message that includes sender and receiver numbers and message body
message = client.messages.create(
                 	body="Intruder detected 12",
                 	from_='+12708120109',
                 	to='+923763533781')
#send message
print(message.sid)
```
We need to add two environment variables using CLI before executing this script.
```
export TWILIO_ACCOUNT_SID=ACd08a7ca906445927dca23456
export TWILIO_AUTH_TOKEN=edc2865003616d2a23533r667

```

## EMAIL ALERT
For generating sms alerts we will use the smtplib module of python. Simple Mail Transfer Protocol (SMTP) is a protocol, which handles sending email and routing email between mail servers.  
Python provides a smtplib module, which defines an SMTP client session object that can be used to send mail to any Internet machine with an SMTP or ESMTP listener daemon.

- For sending email use gmail.com server we need to first create an app password from our gmail account. To create this app password follow the steps defined in this [link](https://stackoverflow.com/questions/10147455/how-to-send-an-email-with-gmail-as-provider-using-python/71008714#71008714).

### Python code to generate email alert:
```
import smtplib
sender = 'abc987@gmail.com'
receivers = 'xyz@gmail.com'

message = "INTRUDER DETECTED"
#create smtp object and pass the server name and port address that will be used to send email
smtpObj = smtplib.SMTP('smtp.gmail.com',587)
# start session
smtpObj.starttls()
# login to the sender account with sender gmail id and app password generated using above steps
smtpObj.login(sender,'wioldfkdfejennybldwe')
#send email to receiver email address
smtpObj.sendmail(sender, receivers, message)    	 
print ("Successfully sent email")
```
