import cv2
from ultralytics import YOLO
from PIL import Image
import os
from twilio.rest import Client
import smtplib
from datetime import datetime
import csv

# for sending sms alert
account_sid = os.environ["TWILIO_ACCOUNT_SID"]
auth_token = os.environ["TWILIO_AUTH_TOKEN"]
client = Client(account_sid, auth_token)

# for sending email alert
sender = 'alaroobaliaqat987@gmail.com'
receivers = 'zaryabliaqat123@gmail.com'
email_body = "INTRUDER DETECTED 1"

smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
smtpObj.starttls()


def box_label(image, box, label='', color=(128, 128, 128), txt_color=(255, 255, 255)):
    lw = max(round(sum(image.shape) / 2 * 0.003), 2)
    p1, p2 = (int(box[0]), int(box[1])), (int(box[2]), int(box[3]))
    cv2.rectangle(image, p1, p2, color, thickness=lw, lineType=cv2.LINE_AA)
    if label:
        tf = max(lw - 1, 1)  # font thickness
        w, h = cv2.getTextSize(label, 0, fontScale=lw / 3, thickness=tf)[0]  # text width, height
        outside = p1[1] - h >= 3
        p2 = p1[0] + w, p1[1] - h - 3 if outside else p1[1] + h + 3
        cv2.rectangle(image, p1, p2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(image,
                    label, (p1[0], p1[1] - 2 if outside else p1[1] + h + 2),
                    0,
                    lw / 3,
                    txt_color,
                    thickness=tf,
                    lineType=cv2.LINE_AA)


def plot_bboxes(image, boxes, labels=[], colors=[], score=True, conf=None):
    # Define COCO Labels
    if labels == []:
        labels = {0: u'__background__', 1: u'person', 2: u'bicycle', 3: u'car', 4: u'motorcycle', 5: u'airplane',
                  6: u'bus', 7: u'train', 8: u'truck', 9: u'boat', 10: u'traffic light', 11: u'fire hydrant',
                  12: u'stop sign', 13: u'parking meter', 14: u'bench', 15: u'bird', 16: u'cat', 17: u'dog',
                  18: u'horse', 19: u'sheep', 20: u'cow', 21: u'elephant', 22: u'bear', 23: u'zebra', 24: u'giraffe',
                  25: u'backpack', 26: u'umbrella', 27: u'handbag', 28: u'tie', 29: u'suitcase', 30: u'frisbee',
                  31: u'skis', 32: u'snowboard', 33: u'sports ball', 34: u'kite', 35: u'baseball bat',
                  36: u'baseball glove', 37: u'skateboard', 38: u'surfboard', 39: u'tennis racket', 40: u'bottle',
                  41: u'wine glass', 42: u'cup', 43: u'fork', 44: u'knife', 45: u'spoon', 46: u'bowl', 47: u'banana',
                  48: u'apple', 49: u'sandwich', 50: u'orange', 51: u'broccoli', 52: u'carrot', 53: u'hot dog',
                  54: u'pizza', 55: u'donut', 56: u'cake', 57: u'chair', 58: u'couch', 59: u'potted plant', 60: u'bed',
                  61: u'dining table', 62: u'toilet', 63: u'tv', 64: u'laptop', 65: u'mouse', 66: u'remote',
                  67: u'keyboard', 68: u'cell phone', 69: u'microwave', 70: u'oven', 71: u'toaster', 72: u'sink',
                  73: u'refrigerator', 74: u'book', 75: u'clock', 76: u'vase', 77: u'scissors', 78: u'teddy bear',
                  79: u'hair drier', 80: u'toothbrush'}
    # Define colors
    if colors == []:
        colors = [(89, 161, 197), (67, 161, 255), (19, 222, 24), (186, 55, 2), (167, 146, 11), (190, 76, 98),
                  (130, 172, 179), (115, 209, 128), (204, 79, 135), (136, 126, 185), (209, 213, 45), (44, 52, 10),
                  (101, 158, 121), (179, 124, 12), (25, 33, 189), (45, 115, 11), (73, 197, 184), (62, 225, 221),
                  (32, 46, 52), (20, 165, 16), (54, 15, 57), (12, 150, 9), (10, 46, 99), (94, 89, 46), (48, 37, 106),
                  (42, 10, 96), (7, 164, 128), (98, 213, 120), (40, 5, 219), (54, 25, 150), (251, 74, 172),
                  (0, 236, 196), (21, 104, 190), (226, 74, 232), (120, 67, 25), (191, 106, 197), (8, 15, 134),
                  (21, 2, 1), (142, 63, 109), (133, 148, 146), (187, 77, 253), (155, 22, 122), (218, 130, 77),
                  (164, 102, 79), (43, 152, 125), (185, 124, 151), (95, 159, 238), (128, 89, 85), (228, 6, 60),
                  (6, 41, 210), (11, 1, 133), (30, 96, 58), (230, 136, 109), (126, 45, 174), (164, 63, 165),
                  (32, 111, 29), (232, 40, 70), (55, 31, 198), (148, 211, 129), (10, 186, 211), (181, 201, 94),
                  (55, 35, 92), (129, 140, 233), (70, 250, 116), (61, 209, 152), (216, 21, 138), (100, 0, 176),
                  (3, 42, 70), (151, 13, 44), (216, 102, 88), (125, 216, 93), (171, 236, 47), (253, 127, 103),
                  (205, 137, 244), (193, 137, 224), (36, 152, 214), (17, 50, 238), (154, 165, 67), (114, 129, 60),
                  (119, 24, 48), (73, 8, 110)]

    # plot each boxes
    for box in boxes:
        label = labels[int(box[-1]) + 1]
        print("BOX LABEL: ",label)
        if label == 'person'and int(round(100 * float(box[-2]), 1))>=80:

            # for cls in result.boxes.cls:
            #     print("CLASS",result.names[int(cls)])
            #     class_name=result.names[int(cls)]
            #     if class_name=='person':
            # print("Person label detected")
            # add score in label if score=True
            if score:
                print("Score",score)
                label = labels[int(box[-1]) + 1] + " " + str(round(100 * float(box[-2]), 1)) + "%"
                confidence=str(round(100 * float(box[-2]), 1))
                print("CONF:",confidence)
                print("1_Label: ",label)
            else:
                label = labels[int(box[-1]) + 1]
                # print("2_Label: ",label)
            # filter every box under conf threshold if conf threshold setted
            if conf:
                if box[-2] > conf:
                    color = colors[int(box[-1])]
                    box_label(image, box, label, color)
                # print("IF RUNS")
            else:
                print("CONF NOT SET")
                color = colors[int(box[-1])]
                #print("label",label)
                box_label(image, box, label, color)
                # print("ELSE RUNS")
        else:
            continue
            # print("Skipped")
    # show image
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return image


# Load a pretrained YOLO model (recommended for training)
print("Load model")
model = YOLO('yolov8n.pt')
# declare required variables
count = 0
frame_cnt = 0

# find weekday
dt = datetime.now()
weekday = dt.strftime('%A')


def read_data_by_index(csv_file, index):
    # Open the CSV file
    with open(csv_file, 'r') as file:
        # Create a CSV reader object
        csv_reader = csv.reader(file)

        # Skip the header row if it exists
        header = next(csv_reader, None)

        # Iterate over each row in the CSV file
        for row in csv_reader:
            # Access the value at the specified index
            if row[0] == weekday:
                print(row)
                # print(type(row[1]))
                start_time = row[1]
                end_time = row[2]
                frame_limit = row[3]
    return str(start_time), str(end_time), frame_limit
    # Process the value (e.g., print or store it)


# Example usage
csv_file = 'schedule.csv'
index = 0
start_time, end_time, frame_limit = read_data_by_index(csv_file, index)
print("start time", type(start_time))
print("frame_limit", type(frame_limit))

# parse date into date type
# Use datetime.strptime to parse the time string into a datetime object
time_obj1 = datetime.strptime(start_time, "%H:%M:%S").time()
time_obj2 = datetime.strptime(end_time, "%H:%M:%S").time()
print(type(time_obj1))  # Output: <class 'datetime.time'>
print("start time", time_obj1)
start_time = time_obj1
end_time = time_obj2
# Get the current time
current_time = datetime.now().time()

if start_time <= current_time <= end_time or end_time < start_time <= current_time:
    print("The current time lies within the defined interval.")
    res = True
else:
    print("The current time is outside the defined interval.")
    res = False

print(res)
# print("Start capturing video frames from webcam")
if res:
    print("true")
    #video = cv2.VideoCapture(0)
    video = cv2.VideoCapture('Join_file_145051288.mp4')
else:
    print("false")
    exit()
if not video.isOpened():
    print("Error opening video file.")
    exit()
while True:
    ret, frame = video.read()
    frame_cnt = frame_cnt + 1
    print("Frame number:", frame_cnt)
    # print("Frame shape:",frame.shape)
    if ret:
        print("Frame found")
        results = model(frame)
        # frame = frame.resize((1080,810,1))
        # cv2.imshow("Webcam feed",frame)
        # print("Detection results:", results)
        # print("BBOX:", results[0].boxes.data)

        draw_bbox_img = plot_bboxes(frame, results[0].boxes.data, score=True)

        # imageName = "saved_frame2_{}.png".format(frame_cnt)
        # cv2.imwrite(imageName,draw_bbox_img)

        cv2.imshow("Results", draw_bbox_img)
        for result in results:
            # print("Result values: ",result)
            # print(result.boxes)
            # print("CLS:",result.boxes.cls)
            for cls in result.boxes.cls:
                # print("CLASS",result.names[int(cls)])
                class_name = result.names[int(cls)]
                if class_name == 'person':
                    count = count + 1
                    print("Person's count:", count)
                    #if count == int(frame_limit):
                    if count == 80:
                        print("**********************CONDITION TRUE*******************")
                        print("Generate sms alert. ", count)
                        message = client.messages.create(body="Intruder detected 1", from_='+12708120908',
                                                         to='+923161772965')
                        print(message.sid)

                        # for sending email alert
                        print("Generate email alert")
                        smtpObj.login(sender, 'wiolmmejeoybldwe')
                        smtpObj.sendmail(sender, receivers, email_body)
                        print("Successfully sent email")

                        img_name = "opencv_frame1_{}.png".format(frame_cnt)
                        cv2.imwrite("/home/arooba/ultralytics/detection_results/{}".format(img_name), draw_bbox_img)
                        count = 0
        # if frame_cnt==60:
        # break
        # waits for user to press any key
        # (this is necessary to avoid Python kernel form crashing)
        c = cv2.waitKey(1)
        if c == 27:
            break
    if not ret:
        print("frame not found")
        break
# closing all open windows
video.release()
cv2.destroyAllWindows()

"""
import cv2
video = cv2.VideoCapture('Join_file_145051288.mp4',0)
fps = video.get(cv2.CAP_PROP_FPS)
print('frames per second =',fps)

# Check if the video file was successfully opened
if not video.isOpened():
    print("Error opening video file.")
    exit()
while True:
   ret, frame = video.read()
   if ret:
      print("frame found")
      cv2.imshow("image",frame)
      c=cv2.waitKey(1)
      if c==27:
          break
   if not ret:
       break
video.release()
cv2.destroyAllWindows()
"""