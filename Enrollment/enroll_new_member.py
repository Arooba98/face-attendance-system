import cv2

from tensorflow.keras.applications.resnet50 import ResNet50,preprocess_input, decode_predictions
from tensorflow.keras.preprocessing import image
import numpy as np
import pandas as pd
from tqdm.auto import tqdm
import os

def return_image_embedding(model,img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    preds = model.predict(x)
    curr_df = pd.DataFrame(preds[0]).T
    return curr_df
model = ResNet50(include_top=False, weights='imagenet', pooling='avg')

embedding_df = pd.DataFrame()

cap = cv2.VideoCapture(0)
while True:
    ret, img = cap.read()
    cv2.imshow("image", img)
    c = cv2.waitKey(1)
    if c == ord('a'):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # read the haarcascade to detect the faces in an image
        face_cascade = cv2.CascadeClassifier('/home/arooba/ultralytics/image_embeddings/haarcascade_frontalface_default.xml')
        # detects faces in the input image
        faces = face_cascade.detectMultiScale(gray, 1.3, 4)
        print('Number of detected faces:', len(faces))
        # loop over all detected faces
        if len(faces) > 0:
            for i, (x, y, w, h) in enumerate(faces):
                # To draw a rectangle in a face
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 255), 2)
                face = img[y:y + h, x:x + w]
                print("face shape: ", face.shape)
                name = input("Enter your name: ")
                image_name = name + '.jpg'
                cv2.imwrite(image_name, face)
                img_path='/home/arooba/ultralytics/image_embeddings/'+image_name
                curr_df = return_image_embedding(model,img_path)
                curr_df['image'] = image_name 
                embedding_df = pd.concat([embedding_df,curr_df],ignore_index=True)
                embedding_df.to_csv("face_embeddings.csv", index=False)
                print(embedding_df)
        
    if c == 27:
        break
cap.release()
cv2.destroyAllWindows()
print(embedding_df)










